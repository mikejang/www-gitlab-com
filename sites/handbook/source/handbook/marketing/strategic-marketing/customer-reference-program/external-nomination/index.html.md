---
layout: handbook-page-toc
title: "Customer Reference Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## External Customer reference process at GitLab
We have an [external landing page](https://page.gitlab.com/reference.html) that is integrated into customer communications to promote customers to register their interest in joining our Reference Program. 

Process for managing these responses is outlined [here](https://docs.google.com/presentation/d/1iK34NOTLi_Nyq3wyOZQwhPj_D7gU7zRyr2GCCmWMksM/edit#slide=id.gd5ed1e508c_0_0)


## Process for managing customer responses 



## Process for managing Open Source members responses



## Process for managing Education Program members responses 
