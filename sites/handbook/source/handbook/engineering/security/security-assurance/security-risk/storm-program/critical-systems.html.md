---
layout: handbook-page-toc
title: "Critical System Tiering Methodology"
description: "The purpose of the Critical Systems Tiering Methodology is to support GitLab in identifying and understanding the specific systems utilized across the organization that are considered essential to order to maintain operations."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

<!--HTML Parser Markup-->
{::options parse_block_html="true" /}

# Critical Systems Tiering Methodolgy

## Purpose

The purpose of the Critical Systems Tiering Methodology is to support GitLab in identifying and understanding the specific systems utilized across the organization that are considered essential to order to maintain operations. Ultimately, this provides GitLab with a mechanism to take a proactive approach to comprehensive risk management which considers all risks, such as information security and priavacy risks, impacting business operations across the organization. Additionally, by classifying systems into specific tiers, GitLab will be in a better position to appropriately prioritze risk mitigation activities and tailor internal controls based on a system's related tier.

## Scope

The critical systems tiering methodology is applicable to all systems utilized across GitLab which are tracked within the [Tech Stack](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml) in order to ensure that all systems are vetted completely and accurately using a consistent and standardized methodology.

GitLab has taken an iterative approach to rolling out this methodology across the tech stack, beginning with systems that have been identified as being in-scope for external compliance audits. Over the course of FY22, this methodology will be applied to all systems within the tech stack. Once this has been accomplished, an annual review will be performed to to ascertain that each system's assigned tier is still accurate. This work is being performed in conjunction with the formal [Business Impact Analysis (BIA)](/handbook/engineering/security/security-assurance/security-risk/storm-program/business-impact-analysis.html) being executed in FY22. 

## Roles and Responisbilities

|Role|Responsibility|
|----------|------------------------------|
|[Security Risk Team](/handbook/engineering/security/security-assurance/security-risk/)|Executes an annual review of critical system tiers and makes adjustments as necessary. Owns the Critical System Tiering Methodolgy and supports the identification of and assignment of a critical system tier as needed.|
|[IT Compliance](/handbook/business-technology/it-compliance/)|Supports defining of critical system tier in conjunction with the Security Risk Team when new systems are added to the tech stack.|
|Technical System Owners|Provide complete and accurate data about the systems that they own so that an accurate tier is assigned.|

## Critical System Tiering Procedure

Defining what a critical system means at GitLab can be complex given the nature of our environment and the amount of integrations that exist across the many systems that are used to carry out business activities. In order to remain objective, the inputs used to determine system criticality tiers are the following:

1. If the system experienced a disruption or outage for less than 24 hours, would there be an impact to one or both of the following:
   - The GitLab.com SaaS offering 
   - Critical internal business operations
2. Does the system integrate with other systems OR is the system utilized to facilitate/execute integrations (i.e. like a job scheduling and execution tool)?

Once the information is obtained, it is reviewed by the Security Risk and/or IT Compliance Team to determine which system tier should be assigned to the a system. The guidelines used to assign a tier are described in the [Determining Critical System Tiers](#determining-critical-system-tiers) section below.


<div class="panel panel-gitlab-orange">
**Why do we differentiate between impact to the SaaS product vs Critical internal business operations?**
{: .panel-heading}
<div class="panel-body">

Critical systems tiers are meant to be leveraged as a driver for prioritizing work that impacts a large number of systems. Utilizing the system tiers provide a meaningful mechanism to prioritize work for systems which are essential to GitLab and it's customers. Furthermore, because the core service offering is GitLab.com, systems which have an impact on GitLab's ability to maintain/sustain the continued delivery of GitLab.com are given their own dedicated tier, _Tier 1 Product_, because of potential impact to all SaaS customers.

</div>
</div>

### Determining Critical System Tiers

Systems are assigned a critical system tier based on the following matrix:

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;margin:0px auto;}
.tg td{border-color:black;border-style:solid;border-width:1px;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-zqun{background-color:#ffffff;color:#000000;font-weight:bold;text-align:center;vertical-align:middle}
.tg .tg-knp3{background-color:#6e49cb;border-color:#000000;color:#ffffff !important;;
  text-align:center;vertical-align:middle}
.tg .tg-clye{background-color:#380d75;color:#ffffff;font-weight:bold;text-align:center;vertical-align:middle}
.tg .tg-fecx{background-color:#cccccc;color:#000000;font-weight:bold;text-align:center;vertical-align:middle}
.tg .tg-cc97{background-color:#380d75;color:#ffffff;text-align:center;vertical-align:middle}
.tg .tg-dxvi{background-color:#6e49cb;color:#ffffff;font-weight:bold;text-align:center;vertical-align:middle}
.tg .tg-e02t{background-color:#ffffff;border-color:#000000;color:#000000 !important;;
  font-weight:bold;text-align:center;vertical-align:middle}
.tg .tg-9hzb{background-color:#FFF;font-weight:bold;text-align:center;vertical-align:top}
</style>
<table class="tg">
<colgroup>
<col style="width: 100px">
<col style="width: 200px">
<col style="width: 200px">
<col style="width: 43px">
<col style="width: 200px">
</colgroup>
<tbody>
  <tr>
    <td class="tg-clye" rowspan="2">Critical System Tier <span style="color:#DB3B21">*</span></td>
    <td class="tg-clye" colspan="2">If the system experienced a disruption or outage for less than 24 hours, would there be an impact to the GitLab.com SaaS offering and/or internal critical business processes?</td>
    <td class="tg-fecx" rowspan="9">AND<br><br><br><br><br></td>
    <td class="tg-cc97" rowspan="2"><span style="font-weight:600;font-style:normal">Does the system integrate with other systems and/or is the system utilized to facilitate/execute integrations (i.e. like a job scheduling and execution tool)</span></td>
  </tr>
  <tr>
    <td class="tg-dxvi">GitLab.com SaaS Impact</td>
    <td class="tg-knp3"><span style="font-weight:600;font-style:normal">Critical Internal Business Operations Impact (e.g. finance/accounting operations, payroll, sales, etc.)</span></td>
  </tr>
  <tr>
    <td class="tg-e02t" rowspan="3">Tier 1 Product<span style="color:#DB3B21">**</span></td>
    <td class="tg-zqun">YES</td>
    <td class="tg-e02t">YES</td>
    <td class="tg-e02t">YES</td>
  </tr>
  <tr>
    <td class="tg-zqun">YES</td>
    <td class="tg-zqun">YES</td>
    <td class="tg-zqun">NO</td>
  </tr>
  <tr>
    <td class="tg-9hzb">YES</td>
    <td class="tg-9hzb">NO</td>
    <td class="tg-9hzb">NO</td>
  </tr>
  <tr>
    <td class="tg-zqun">Tier 1 Business</td>
    <td class="tg-zqun">NO</td>
    <td class="tg-zqun">YES</td>
    <td class="tg-zqun">YES</td>
  </tr>
  <tr>
    <td class="tg-e02t">Tier 2 Core</td>
    <td class="tg-zqun">NO</td>
    <td class="tg-e02t">YES</td>
    <td class="tg-e02t">NO</td>
  </tr>
  <tr>
    <td class="tg-e02t">Tier 2 Support</td>
    <td class="tg-zqun">NO</td>
    <td class="tg-e02t">NO</td>
    <td class="tg-e02t">YES</td>
  </tr>
  <tr>
    <td class="tg-e02t">Tier 3 / Non-critical</td>
    <td class="tg-zqun">NO</td>
    <td class="tg-e02t">NO</td>
    <td class="tg-e02t">NO</td>
  </tr>
</tbody>
</table>
<br/>

<div class="panel panel-gitlab-purple">
**Notes**
{: .panel-heading}
<div class="panel-body">

<span style="color:#DB3B21;"><b>\*</b></span> As an extension of tiering methodology, the [Data Classification Standard](/handbook/engineering/security/data-classification-standard.html) prescribes **specific** [Security and Privacy](/handbook/engineering/security/data-classification-standard.html#security-and-privacy-controls) control requirements for each data classification level. These requirements should be followed based on a system's data classification, regardless of the system's tier.
{: .note}

<span style="color:#DB3B21;"><b>\**</b></span> By default, any system that contains <b>RED Data</b> per the [Data Classification Standard](/handbook/engineering/security/data-classification-standard.html#red) will be a **Tier 1 Product** system. This is explicitly due to the fact that this type of data is customer owned and uploaded data and as such, has been deemed to be critical in nature.
{: .note}

</div>
</div>

### Why does GitLab need this methodology?

Tiering systems utilized across GitLab enables team members to make decisions on prioritizing work related to a specific system(s) based on the assigned tier. As an example, if multiple risks have been identified over a wide variety of systems and require remediation, impacted team members can leverage the assigned critical system tiers to make a decision on how to prioritize remediation activities. This methodology will also provide GitLab with a mechanism to easily identify those systems which are most critical across the organization so that we can proactively protect and secure these systems.

### Maintaining Critical System Tiers

A critical system assessment is performed on an annual cadence in alignment with the [StORM annual risk assessment process](/handbook/engineering/security/security-assurance/security-risk/storm-program/index.html) to validate existing systems in GitLab’s environment and make adjustments to assigned tiers accordingly. A system's assigned tier can be found in the [tech_stack.yml file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml).


## Exceptions

Systems that are exempt from this methodology include any system which carries a data classification of Green. All remaining systems which store or process YELLOW, ORANGE, or RED data are required to have a critical system tier assigned. Data classification will be validated to corroborate that the data stored or processed by the system is truly green data, per the [Data Classification Standard](/handbook/engineering/security/data-classification-standard.html#green).

## References

* [Business Impact Analysis](/handbook/engineering/security/security-assurance/security-risk/storm-program/business-impact-analysis.html)
* [Data Classification Standard](/handbook/engineering/security/data-classification-standard.html)
